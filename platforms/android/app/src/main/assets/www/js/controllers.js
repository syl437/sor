angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $rootScope,$timeout,$localStorage,$ionicActionSheet,$state,$ionicSideMenuDelegate) {


	$scope.$on('$ionicView.enter', function(e) 
	{
		$scope.EmlopyeeMonth = $rootScope.MonthEmlopyee;
		$scope.MonthEmlopyeePlace = $rootScope.MonthEmlopyeePlace;
		$scope.NavigateBlog = function(blogId, articlId)
		{
			if(articlId == -1)
				$state.go('app.employeemonth');
			else
			{
				console.log("00 : " + blogId + " : " + articlId )
				$rootScope.selectedBlogCategory = blogId;
				$scope.articles = $rootScope.Blogs[$rootScope.selectedBlogCategory].articles;
				console.log($scope.articles)
				$scope.id = articlId;
				console.log("01 : " + $rootScope.selectedBlogCategory + " : " + $scope.id  )
				window.location.href = "#/app/blogpost/" + $scope.id;
			}	
		}

		$scope.MenuClick = function() 
		{
			if ($rootScope.employee =="0") 
				$state.go('app.main');
			else
				$state.go('app.mainemployee');
		}
		
		$scope.checkLoggedIn = function(type) 
		{
			if ($localStorage.username) 
			{
				//$ionicSideMenuDelegate.toggleLeft();
				$rootScope.employee = 1;
				$state.go('app.mainemployee');
			}
			else 
				$state.go('app.login');
		}

		$scope.logout = function(type)  
		{
			//$ionicSideMenuDelegate.toggleLeft();			
			$rootScope.employee = 0;
			$state.go('app.main');		
		}

		$scope.navigateUrl = function (Path,Num) 
		{
			window.location.href = Path+String(Num);
		}
		
		$scope.NavigateCatPage = function(Page,CatType)
		{
			window.location.href = Page;
			$rootScope.CatType = CatType;
		}

	
  
	  $scope.aboutPage = function() 
	  { 
		window.location.href = "#/app/about";
	  }
  
  });
})


.controller('About', function($scope,$http,$rootScope,$timeout,$ionicLoading,$sce,$ionicPlatform,$timeout)
{
	
	//$ionicSideMenuDelegate.canDragContent(false);
	
  mixpanel.track("עמוד אודות");
  
  if (window.cordova)
  {
            
	$timeout(function() 
	{
		 var eventName = "עמוד אודות";	 
		 //var eventValues = {};
		 //window.plugins.appsFlyer.trackEvent(eventName);
	}, 3000);
	  
  }
		
	
	$ionicLoading.show({
      template: 'טוען...',
	  noBackdrop : true,
      duration : 10000
    });
	

  $scope.$on('imageloaded', function(events, args){
	$ionicLoading.hide();
  })
 
  $scope.$on('$ionicView.enter', function(e) {

     $timeout(function() 
	 {
	 
		$scope.imageurl = $rootScope.host;
		$scope.aboutimage = $rootScope.aboutimage;
		$scope.abouttitle = $rootScope.abouttitle;
		$scope.newtitle = $sce.trustAsHtml($scope.abouttitle);
		$scope.abouttext = $rootScope.abouttext;	 
		$scope.trustedHtml = $sce.trustAsHtml($scope.abouttext);
		//console.log($scope.abouttext)
  
	}, 600); 
	
	}); 


})


.controller('Feedback', function($scope,$http,$rootScope,$ionicPopup,$ionicPlatform,$timeout)
{
	mixpanel.track("עמוד שלח משוב");

	if (window.cordova)
	{
	
		$timeout(function() {
			 
		 var eventName = "שלח משוב";
		 
		 //var eventValues = {};
		 //window.plugins.appsFlyer.trackEvent(eventName);
		 }, 3000);		  
	}


	
	$scope.Empty = 'img/new/s2.png';
	$scope.Full = 'img/new/s1.png';
	$scope.RateArray = new Array();
	
	$scope.openQuestion = 
	{
		"openquestion" : ''
	}
	
	
	$scope.sendFeedback  = function() 
	{ 
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		$scope.Answers = '';
		$scope.NotAnswerQuestion = 0;
		
		for(i=0;i<$scope.RateArray.length;i++)
		{
			if($scope.RateArray[i]["val"] != 0)
			$scope.Answers += String($scope.RateArray[i]["val"]+',')
			else
			$scope.NotAnswerQuestion = i+1;
		}
		console.log($scope.Answers + " : " + $scope.openQuestion.openquestion)
		data = 
		{
			"answers" : $scope.Answers,
			"openquestion" : $scope.openQuestion.openquestion
		}
		
		$scope.Answers = ''
		
		if($scope.NotAnswerQuestion == 0)
		{
			$http.post($rootScope.host+'/send_Feedback.php',data)
			.success(function(data, status, headers, config)
			{
				$scope.openQuestion = '';
				
				$ionicPopup.alert({
				 title: 'תודה רבה, פרטי המשוב התקבלו בהצלחה, הינך מעובר לעמוד הראשי',
				buttons: [{
					text: 'אשר',
					type: 'button-positive',
				  }]
				});
   
			window.location.href = "#/app/main";
			})
			.error(function(data, status, headers, config)
			{
				//alert("f : " +data.responseText)
				//console.log('error : ' + data);
				//console.log('error : ' + data);
			});
		}
		else
		{
			
			$ionicPopup.alert({
			 title: "לא דירגת את שאלה מספר " + $scope.NotAnswerQuestion,
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
			});	
		}
	}
	
	$http.get($rootScope.host+'getQuestions.php')
    .success(function(data, status, headers, config)
     {
		$scope.Questions = data.response;
		
		for(i=0;i<$scope.Questions.length;i++)
		{
			$scope.Rate = new Object();
			$scope.Rate =
			{
				"rate1" : $scope.Empty,
				"rate2" : $scope.Empty,
				"rate3" : $scope.Empty,
				"rate4" : $scope.Empty,
				"rate5" : $scope.Empty,
				"val" : 0
			}

			$scope.RateArray[i] = $scope.Rate;
		}
		
		console.log($scope.RateArray)
     })
     .error(function(data, status, headers, config)
     {
     });
	 
	
	 
	 $scope.starClick = function (nm1,nm2)
	 {
		 for(i=1;i<=nm1;i++)
		 {
			 console.log("Full : " + i)
			 $scope.RateArray[nm2]["rate"+i] = $scope.Full;
		 }
		 
		 for(i=5; i>nm1;i--)
		 {
			console.log("Empty : " + i)
			$scope.RateArray[nm2]["rate"+i] = $scope.Empty; 
		 }
		 
		 $scope.RateArray[nm2]["val"] = nm1;
		 console.log($scope.RateArray)
		 console.log($scope.RateArray[nm2]["val"] + " : " + $scope.nm1)
	 }
})

.controller('Recommended', function($scope,$http,$rootScope,$ionicPlatform,$timeout)
{	
	mixpanel.track("עמוד אפליקציות ממולצות");

	if (window.cordova)
	{
	
		$timeout(function() {
		 
		 var eventName = "עמוד אפליקציות ממולצות";
		 
		 //var eventValues = {};
		 //window.plugins.appsFlyer.trackEvent(eventName);
		 }, 3000);		  
	}


	$scope.imagePath = $rootScope.host;
	$http.get($rootScope.host+'/getRecommended.php')
	.success(function(data, status, headers, config)
	{
		$scope.recommended = data.response;
		console.log($scope.recommended)
		
	})
	.error(function(data, status, headers, config)
	{
		
	});	
	
	$scope.RecommendedLink = function(link)
	{
		if (link)
			window.location.href= link;
		
	}
})

.controller('Contacts', function($scope,$http,$rootScope,$stateParams,$ionicScrollDelegate,$ionicPlatform,$timeout)
{		 
  //$scope.$on('$ionicView.enter', function(e) 
  //{
	mixpanel.track("עמוד אנשי קשר");

	if (window.cordova)
	{
	
		$timeout(function() {
				 
		 var eventName = "אנשי קשר";
		 
		 //var eventValues = {};
		 //window.plugins.appsFlyer.trackEvent(eventName);
		 }, 3000);
	}


	
	$scope.contactInput = '';
	$scope.contacts = $rootScope.contacts;
	$scope.byContactLogin = function()
	{
		return function(row)
		{
			return row.employee == $rootScope.employee || row.employee == 2;
		}
	}


	$scope.DialPhone = function(phone)
	{
		window.open('tel:' + phone, '_system');
	}
	
	$scope.BlurContacts = function()
	{
		$ionicScrollDelegate.scrollTop();
	}

})



.controller('Navigation', function($scope,$http,$rootScope,$stateParams,$ionicScrollDelegate,$cordovaBarcodeScanner,$ionicPopup,$ionicPlatform,$timeout)
{	
	 
	mixpanel.track("עמוד ניווט");

	if (window.cordova)
	{
	
		$timeout(function() {
			 
		 var eventName = "ניווט";
		 
		 //var eventValues = {};
		 //window.plugins.appsFlyer.trackEvent(eventName);
		 }, 3000);

	}


	
	$scope.navigationInput = '';
	$scope.NavigationId = '';
	
	$scope.FocusField = function()
	{
		// $ionicScrollDelegate.scrollTop();
		//$scope.$broadcast('scroll.scrollTop');
		//$anchorScroll();
		//  $window.scrollTo(0,0);
	}
	
	$scope.getData = function(index)
	{			
		if (index == 1)
		{
			$scope.ShowDevision = true;
			$scope.ShowBuildings = false;
			$scope.SearchByName = "חפש מחלקה";
			$scope.NavigationType = 0;
			$scope.navigation = $rootScope.navigation;
			$ionicScrollDelegate.scrollTop();
		}
		else 
		{
			$scope.ShowDevision = false;
			$scope.ShowBuildings = true;
			$scope.SearchByName = "חפש בניין";
			$scope.NavigationType = 1;
			$scope.navigation = $rootScope.buildings;
			$ionicScrollDelegate.scrollTop();
		}
		
	}
	
	
	$scope.getData(1);
	
	
	$scope.navigateUrl = function(index,buildingnumber)
	{
		if ($scope.NavigationType == 0)
			window.location.href = "#/app/navigation_info/"+index;
		else
			window.location.href = "#/app/buildings/"+buildingnumber;
		
	}
	
	$scope.BlurNavigation = function()
	{
		$ionicScrollDelegate.scrollTop();
	}
	
	$scope.scanQR = function()
	{

	  cordova.plugins.barcodeScanner.scan(
	  function (result) 
	  {

			 $scope.buildingsplit = result.text.split("=");
			 $scope.buildingcode = $scope.buildingsplit[1].substring(1);
			  
			for (i = 0; i < $rootScope.navigation.length; i++) 
			{ 
				if ($rootScope.navigation[i].building_name_signing == $scope.buildingcode)
					$scope.NavigationId = $rootScope.navigation[i].id;
			}
			
			if ($scope.NavigationId)
				window.location.href = "#/app/navigation_info/"+$scope.NavigationId;
			else
			{
				$ionicPopup.alert({
				 title: "יש נמצאו נתונים , יש לסרוק את קוד ה QR שוב.",
				buttons: [{
					text: 'אשר',
					type: 'button-positive',
				  }]
				});
			}
			
		  /*
		  alert("We got a barcode\n" +
				"Result: " + result.text + "\n" +
				"Format: " + result.format + "\n" +
				"Cancelled: " + result.cancelled);
		*/
	  }, 
	  function (error) 
	  {
		  
		$ionicPopup.alert({
		title: "שגיאה בסריקה: " + error,
		buttons: [{
		text: 'אשר',
		type: 'button-positive',
		}]
		});
	  }
   );
	}
	
})


.controller('NavigationBuilding', function($scope,$http,$rootScope,$stateParams,$ionicScrollDelegate,$ionicPlatform,$timeout)
{	

	mixpanel.track("עמוד ניווט בניינים");

	if (window.cordova)
	{
	
		$timeout(function() {
		 
		 var eventName = "ניווט בניינים";
		 //var eventValues = {};
		 //window.plugins.appsFlyer.trackEvent(eventName);
		 }, 3000);

	}

		
	$scope.buildingInput = '';

	$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
	
	data_params = 
	{
		"building_number" : $stateParams.ItemId
	}
	//console.log(send_params)
	$http.post($rootScope.host+'/getBuildingNumber.php',data_params)
	.success(function(data, status, headers, config)
	{
		$scope.buildings = data.response;
		//console.log("buildings");
		//console.log($scope.buildings)
		//alert ($scope.buildings)
	})
	.error(function(data, status, headers, config)
	{

	});
			
	$scope.BlurBuildings = function()
	{
		$ionicScrollDelegate.scrollTop();
	}
		$scope.navigateUrl = function (Path,Num) 
	{
		window.location.href = Path+String(Num);
	}
})


.controller('NavigationInfo', function($scope,$http,$rootScope,$stateParams,$ionicActionSheet,$ionicPlatform,$timeout)
{	
	mixpanel.track("עמוד פרטי ניווט");

	if (window.cordova)
	{
	
		$timeout(function() {
		 var eventName = "פרטי ניווט";
		 //var eventValues = {};
		 //window.plugins.appsFlyer.trackEvent(eventName);
		 }, 3000);
	}

	$scope.NavigationInfo = "";
	//$stateParams.ItemId;
	for(i=0;i<$rootScope.navigation.length;i++)
	{
		if ($rootScope.navigation[i].id == $stateParams.ItemId)
		{
			$scope.NavigationInfo = $rootScope.navigation[i];
		}				
	}
	
	
	$scope.buildingname = $scope.NavigationInfo.building_name;
	$scope.devision = $scope.NavigationInfo.devision;
	$scope.floor = $scope.NavigationInfo.floor;
	$scope.area = $scope.NavigationInfo.area;
	$scope.how_to_arrive = $scope.NavigationInfo.how_to_arrive;
	$scope.mapimage = $scope.NavigationInfo.building_name_signing;
	
	
	$scope.showSite = function(buildingnumber)
	{
		
		var hideSheet = $ionicActionSheet.show({
		 buttons: [
		   { text: 'שער וינגט' },
		   { text: 'שער רגר' },
		   { text: 'שער בן גוריון' },
		   { text: 'שער ארלוזורוב' },
		   { text: 'שער מרפאות' },	
		 //  { text: 'מידע על הבניין' },		   			   
		   { text: 'חזור'}
		 ],
		 cancelText: 'ביטול',
		 cssClass: 'social-actionsheet',
		 cancel: function() {
			  return true;
			},
		 buttonClicked: function(index) 
		 {

			if (index == 0)
				iabRef = window.open("http://212.179.90.44:82/pages/navigation.html?route=1"+buildingnumber, '_blank', 'location=yes','toolbar=no');

			else if (index == 1)
				iabRef = window.open("http://212.179.90.44:82/pages/navigation.html?route=2"+buildingnumber, '_blank', 'location=yes','toolbar=no');

			else if (index == 2)
				iabRef = window.open("http://212.179.90.44:82/pages/navigation.html?route=3"+buildingnumber, '_blank', 'location=yes','toolbar=no');
				
			else if (index == 3)
				iabRef = window.open("http://212.179.90.44:82/pages/navigation.html?route=4"+buildingnumber, '_blank', 'location=yes','toolbar=no');
				
			else if (index == 4)
				iabRef = window.open("http://212.179.90.44:82/pages/navigation.html?route=5"+buildingnumber, '_blank', 'location=yes','toolbar=no');
				
		   return true;
		 }});

	}

	$scope.buildingInfo = function(buildingnumber)
	{
		iabRef = window.open("http://212.179.90.44:82/pages/departments.html?route=1"+buildingnumber, '_blank', 'location=yes','toolbar=no');
	}

			
})


.controller('MainController', function($scope,$http,$rootScope,$ionicLoading,$ionicPopup,$ionicSideMenuDelegate,$ionicHistory,$ionicPlatform,$timeout)
{

	$scope.openMenu = function()
	{
		$ionicSideMenuDelegate.toggleLeft();
	}	
	//$scope.newsItems = $rootScope.newsItems//[{news:"hai"},{news:"hello"}];
	console.log("News " ,  $scope.newsItems)
	$ionicSideMenuDelegate.canDragContent(false);


	mixpanel.track("עמוד ראשי");

	if (window.cordova)
	{
		/*
		 $timeout(function() 
		 {

			  var eventName = "עמוד ראשי";
			  //window.plugins.appsFlyer.trackEvent(eventName);

			  
			  if ($rootScope.loadedOnce == 0)
			  {
				  var args = [];
				  var devKey = "KDxeYV8M9CwzgrsKq3fVTf";   // your AppsFlyer devKey
				  args.push(devKey);
				  var userAgent = window.navigator.userAgent.toLowerCase();
				  
				  if (/iphone|ipad|ipod/.test( userAgent )) {
				  var appId = "123456789"; // your ios app id in app store
				  args.push(appId);
				  }
				  window.plugins.appsFlyer.initSdk(args);
				  $rootScope.loadedOnce = 1;
			  }
				  
		 }, 3000);
		 */
			
	}	


	$scope.navTitle='<img class="title-image" src="img/main/ylogo.png" />'
	$scope.rightButton = '<img class="right-button" src="img/main/question.png" />'
	
	//setBackgroundImg();
	LevelOpen = 2;
	//$scope.BackUrl = checkBackUrl();
	//$scope.ginun = data;
	Page_Id = 1;

	



	$scope.openUrl = function(URL)
	{
		iabRef = window.open(URL, '_blank', 'location=yes','toolbar=no');
	}
	
	$scope.navigateUrlMain = function (Path,Num) 
	{	
		window.location.href = Path+String(Num);
	}
	
	$scope.showInfo = function () 
	{
		angular.element('#InfoPopUp').css('visibility','visible');
	}
	
	$scope.hideInfo = function () 
	{
		angular.element('#InfoPopUp').css('visibility','hidden');
	}
	
	$scope.navigateFeatured = function(catagoryid,articleid)
	{
		$rootScope.selectedBlogCategory = catagoryid;
		window.location.href = "#/app/blogpost/" + articleid;
	}
	
	
	$scope.NavigateBlogMain = function(blogId, articlId)
	{
				
		$ionicHistory.nextViewOptions({
		  disableAnimate: true,
		  //disableBack: true
		});
	
		
		if(articlId == -1)
			$state.go('app.employeemonth');
		else
		{
			console.log("00 : " + blogId + " : " + articlId )
			$rootScope.selectedBlogCategory = blogId;
			$scope.articles = $rootScope.Blogs[$rootScope.selectedBlogCategory].articles;
			console.log($scope.articles)
			$scope.id = articlId;
			console.log("01 : " + $rootScope.selectedBlogCategory + " : " + $scope.id  )
			window.location.href = "#/app/blogpost/" + $scope.id;
		}	
	}
		

})


.controller('CatagoryPosts', function($scope,$http,$rootScope,$stateParams,$ionicScrollDelegate,$ionicPlatform,$timeout)
{
	//$scope.$on('$ionicView.enter', function(e) {
	mixpanel.track("עמוד פוסטים מהבלוג");

	if (window.cordova)
	{
	
		$timeout(function() {
		 var eventName = "פוסטים מהבלוג";
		 //var eventValues = {};
		 //window.plugins.appsFlyer.trackEvent(eventName);
		 }, 3000);
	}


	
	$scope.postInput = '';
	$scope.id  = $stateParams.id;
	$rootScope.selectedBlogCategory = parseInt($scope.id); 

	$scope.colorArray = new Array("#377fba","#00ac36");
	$scope.imageurl = $rootScope.host;
	$scope.blogposts = $rootScope.Blogs;	
	
	
	for(i=0;i<$scope.blogposts.length;i++)
	{
		if ($scope.blogposts[i].index == $scope.id)
			$scope.articles = $scope.blogposts[i].articles;
	}	
	
	if ($scope.id == 35)
		$scope.title = 'מרכזי מצוינות';
	else if ($scope.id == 40)
		$scope.title = 'שר"מ';
	

	else
	{
		if ($rootScope.employee == 1)
		{
			if ($rootScope.CatType == 0)
				$scope.title = 'משאבי אנוש';
			if ($rootScope.CatType == 1)
				$scope.title = 'מרדימים';
			if ($rootScope.CatType == 2)
				$scope.title = 'נהלים';
		}
			
		else
			$scope.title = 'חדש ומעניין בסורוקה';		
	}



	$scope.byPostLogin = function()
	{
		return function(row)
		{
				return row.employee == $rootScope.employee;	
		}
	}
	
	$scope.BlurBlog = function()
	{
		$ionicScrollDelegate.scrollTop();
	}
})

.controller('BlogPost', function($scope,$http,$rootScope,$stateParams,$ionicLoading,$sce,$timeout,$ionicPlatform) 
{
	
	$ionicLoading.show({
      template: 'טוען...',
	  noBackdrop : true,
      duration : 10000
    });
	
	mixpanel.track("עמוד פוסט "+ $stateParams.id+" מהבלוג");
		
	if (window.cordova)
	{
	
		$timeout(function() {
		 var eventName = "פוסט מהבלוג";
		 //var eventValues = {};
		 //window.plugins.appsFlyer.trackEvent(eventName);
		 }, 3000);
	}

		

  $scope.$on('imageloaded', function(events, args){
	$ionicLoading.hide();
  })

  
  $scope.$on('$ionicView.enter', function(e) {

     $timeout(function() 
	 {

		console.log("Blog : " + $rootScope.selectedBlogCategory)
		console.log($rootScope.Blogs)
		$scope.id  = $stateParams.id;
		$scope.currentBlog = 0;
		$scope.CategoryIndex = 0;//$rootScope.selectedBlogCategory = $scope.id; 
		
		for(var i=0;i<$rootScope.Blogs.length;i++)
		{
			if($rootScope.selectedBlogCategory == $rootScope.Blogs[i].index)
			{
				$scope.CategoryIndex = i;
				console.log("CategoryIndex : " + i)
			}
		}
		
		
		$scope.blogposts = $rootScope.Blogs;
		$scope.articles = $scope.blogposts[$scope.CategoryIndex].articles;
		
		for(var i=0; i<$scope.articles.length; i++)
		{
			if($scope.articles[i].index == $scope.id)
			{
				$scope.currentBlog = i;
			}
		}
		
		console.log($scope.articles);
		console.log($scope.blogposts)
		//alert ($rootScope.selectedBlogCategory+ " " + $scope.id)
		$scope.imageurl = $rootScope.host;
		$scope.articleindex = $scope.articles[$scope.currentBlog].index;
		//alert ($scope.articleindex)
		$scope.articleimage = $scope.articles[$scope.currentBlog].ArticleImage;
		$scope.articletitle = $scope.articles[$scope.currentBlog].ArticleTitle;
		$scope.articletext = $scope.articles[$scope.currentBlog].ArticleDesc;
		$scope.trustedHtml = $sce.trustAsHtml($scope.articletext);
		$scope.articlecatagory = ''; //$scope.blogposts.CatName;
		$scope.articecatagory = $scope.articles[$scope.currentBlog].CatIndex;
		
		if ($scope.articleindex == 205)
			$scope.title = 'מרכזי מצוינות';

		else if ($scope.articleindex == 28)
			$scope.title = 'שירות לאומי';
		
		else if ($scope.articleindex == 30)
			$scope.title = 'תרומות טרומבוציטים';
		
		else if ($scope.articleindex == 26)
			$scope.title = 'מרפאת מטיילים';

		
		else if ($scope.articecatagory == 35)
			$scope.title = 'מרכזי מצוינות';
		
		else if ($scope.articecatagory == 40)
			$scope.title = 'שר"מ';

		else
		{
			if ($rootScope.employee == 1)
			{
				if ($rootScope.CatType == 0)
					$scope.title = 'משאבי אנוש';
				if ($rootScope.CatType == 1)
					$scope.title = 'מרדימים';
				if ($rootScope.CatType == 2)
					$scope.title = 'נהלים';
			}
				
			else
				$scope.title = 'חדש ומעניין בסורוקה';
		}
		
	}, 300); 
  }); 
  
})

.controller('GeneralPdfCtrl', function($scope,$http,$rootScope,$stateParams,$ionicLoading,$sce,$timeout,$ionicPlatform) 
{
	$scope.title = 'משאבי אנוש';
	$scope.articlecatagory = 'נהלים כללים';
	
	$scope.openPDF = function(id)
	{
		iabRef = window.open($rootScope.host+'pdfs/'+id+'.pdf', '_system', 'location=yes','toolbar=no');

	}
})
.controller('MenuController', function($scope,$http,$rootScope,$stateParams,$timeout) 
{
	if ($rootScope.employee == 1)
	{
		if ($rootScope.CatType == 0)
			$scope.title = 'משאבי אנוש';
		if ($rootScope.CatType == 1)
			$scope.title = 'מרדימים';
		if ($rootScope.CatType == 2)
			$scope.title = 'נהלים';
	}
		
	else
		$scope.title = 'חדש ומעניין בסורוקה';
	
	
		
     $timeout(function() 
	 {

		$scope.imageurl = $rootScope.host;
		$scope.blogposts = $rootScope.Blogs;
		$scope.Employee = $rootScope.employee; 
		console.log($scope.blogposts);
		
		$scope.navigateCategories = function(id)
		{
			if (id == "41")
				window.location.href = "#/app/general_pdf";
			else if (id == "42")
				iabRef = window.open("http://www.health.gov.il/Services/Pages/NoticesAndRegulations.aspx", '_blank', 'location=yes','toolbar=no');
			else
				window.location.href = "#/app/catagoryposts/"+id;
		}
		
		$scope.navigateUrl = function (Path,Num) 
		{
			setTimeout(function(){ window.location.href = Path+String(Num); }, 0);	
		}
		
				
		$scope.byBlogLogin = function()
		{
			return function(row)
			{
				return row.employee == $rootScope.employee;
			}
		}
		
		
		$scope.getIndexPlace = function(ID)
		{
			for(i=0;i<$scope.blogposts.length;i++)
			{
				if($scope.blogposts[i].index == ID)
				return i;
			}
			
			return 0;
		}
	//});
	
	}, 100); 
})




.controller('Youtube', function($scope,$http,$rootScope,$stateParams,$sce,$ionicPlatform,$timeout)
{
	//$scope.MainVideo = '';
	 mixpanel.track("עמוד סורוקה וידאו");

	if (window.cordova)
	{
	
		$timeout(function() 
		{
		 
		 var eventName = "סורוקה וידאו";
		 //var eventValues = {};
		 //window.plugins.appsFlyer.trackEvent(eventName);
		 }, 3000);
	}
	
	 $http.get($rootScope.host+'/getVideos.php')
	.success(function(data, status, headers, config)
	{
		$scope.videos = data.response;
		
		for (i = 0; i < $scope.videos.length; i++) 
		{ 
			if ($scope.videos[i].id == 7)
			{
				 $scope.MainVideo = $sce.trustAsResourceUrl($scope.videos[i].videolink);	
			}
		}
		console.log($scope.videos)
	})
	.error(function(data, status, headers, config)
	{
		
	});	
	
	$scope.openVideo = function(URL)
	{
		iabRef = window.open(URL, '_blank', 'location=yes','toolbar=no');
	}

		
})


.controller('LoginCtrl', function($scope,$http,$rootScope,$stateParams,$localStorage,$state,$ionicPopup,$ionicPlatform,$timeout)
{
	$scope.loginData = 
	{
		"username" : "",
		"password" : ""
	}
	mixpanel.track("עמוד התחברות עובד");

	if (window.cordova)
	{
	
		$timeout(function() {
		 
		 var eventName = "התחברות עובד";
		 
		 //var eventValues = {};
		 //window.plugins.appsFlyer.trackEvent(eventName);
		 }, 3000);
	}

	
	$scope.doLogin = function() 
	{
		
		if ($scope.loginData.username =="")
		{
			$ionicPopup.alert({
			 title: "יש להזין שם משתמש",
			 buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
			});				
		}
		else if ($scope.loginData.password =="")
		{
			$ionicPopup.alert({
			 title: "יש להזין סיסמה",
			 buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
			});				
		}
		else
		{
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		
			send_params = 
			{
				
				"username" : $scope.loginData.username,
				"password" : $scope.loginData.password,
				"send" : 1

			}
			//console.log(send_params)
			$http.post($rootScope.host+'/employee_login.php',send_params)
			.success(function(data, status, headers, config)
			{
				if (data[0].status == 0)
				{
					$ionicPopup.alert({
					 title: "שם משתמש או סיסמה שגוים יש לנסות שוב",
					 buttons: [{
						text: 'אשר',
						type: 'button-positive',
					  }]
					});					
				}
				else
				{
					$localStorage.username =  data[0].userid;
					$rootScope.employee = 1;
					//$state.go('app.mainemployee')
					window.location.href = "#/app/mainemployee";
			
				}

			})
			.error(function(data, status, headers, config)
			{

			});
		}
	}
})

.controller('EmployeeController', function($scope,$http,$rootScope,$stateParams,$localStorage,$state,$ionicSideMenuDelegate,$ionicPlatform,$ionicHistory,$timeout)
{
	$ionicSideMenuDelegate.canDragContent(false);
	
	$scope.openMenu = function()
	{
		$ionicSideMenuDelegate.toggleLeft();
	}	

	$scope.openSite = function(URL)
	{
		iabRef = window.open(URL, '_blank', 'location=yes','toolbar=no');		
	}
	

	
	$ionicPlatform.registerBackButtonAction(function (event) 
	{
		if($rootScope.State == 'app.mainemployee') 
			$rootScope.employee = 0;
		
		$ionicHistory.goBack();
		
	},100);
	

})


.controller('TradingController', function($scope,$http,$rootScope,$stateParams,$localStorage,$state,$ionicPlatform,$timeout)
{
	mixpanel.track("עמוד שוק תן וקח");

	if (window.cordova)
	{
	
	$timeout(function() {
			 
		 var eventName = "שוק תן וקח";
		 
		 //var eventValues = {};
		 //window.plugins.appsFlyer.trackEvent(eventName);
		 }, 3000);
	}
		
	
	$scope.$on('$ionicView.enter', function(e) {

	$scope.imageurl = $rootScope.host;
	
	 $http.get($rootScope.host+'/getTrading.php')
        .success(function(data, status, headers, config)
        {
			$scope.sellitems = data.response;
			console.log($scope.sellitems)
        })
        .error(function(data, status, headers, config)
        {
			
        });	
		
	});	
})



.controller('AccidentController', function($scope,$http,$cordovaCamera,$ionicActionSheet,$rootScope,$stateParams,$localStorage,$state,$ionicPopup,$ionicPlatform,$timeout)
{
	mixpanel.track("עמוד דווח על מפגע");

	if (window.cordova)
	{
	
		$timeout(function() {
		 
		 var eventName = "דווח על מפגע";
		 //var eventValues = {};
		 //window.plugins.appsFlyer.trackEvent(eventName);
		 }, 3000);

	}


		
	$scope.showsubmit = false;
	
	$scope.accidentFields = 
	{
		"name": $localStorage.accidentname,
		"phone": $localStorage.accidentphone,
		"devision": "",
		"room": "",
		"areasSelect": "",
		"inventer": "",
		"desc": ""
	}
	
	$scope.uploadedimage = 'img/main/camera.png';
	
		$scope.onFileSelect = function (file) 
	{
		var fd = new FormData();
        fd.append('fileUPP', file.files[0]);
		$scope.showsubmit = true;		
		$http.post($rootScope.host+'/UploadImg.php', fd, { transformRequest: angular.identity,"headers" : { "Content-Type" : undefined }}).success(function(data) 
		{
			if (data.response.imagepath) {
				$scope.image = data.response.imagepath;
				$scope.showsubmit = false;
				$scope.uploadedimage = $rootScope.host+data.response.imagepath;
			}
         	//alert("p: "+data)  
        }).error(function(data) 
		{
			//alert(data.message); 	
		});
	};
	
	
	$scope.CameraOption = function() 
	{
	   // Show the action sheet
	   var hideSheet = $ionicActionSheet.show({
		 buttons: [
		   { text: 'בחר תמונה מהגלרייה' },
		   { text: 'פתח מצלמה' },
		   { text: 'חזור'}
		 ],
		 cancelText: 'Cancel',
		 cssClass: 'social-actionsheet',
		 cancel: function() {
			  return true;
			},
		 buttonClicked: function(index) {
			 if(index<2)
			 	$scope.takePicture(index);
			 
		   return true;
		 }});
			 // For example's sake, hide the sheet after two seconds
	
		
	};
	
   
   
   
	// destinationType : Camera.DestinationType.DATA_URL,  CAMERA
	$scope.takePicture = function(index) {
		 var options ;
		 
		if(index == 1 )
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI, // Camera.DestinationType.DATA_URL, 
				sourceType : Camera.PictureSourceType.CAMERA, 
				allowEdit : false,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 600,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false
			};
		}
		else
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI, 
				sourceType : Camera.PictureSourceType.PHOTOLIBRARY, 
				allowEdit : false,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 600,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false
			};
		}
	 
        $cordovaCamera.getPicture(options).then(function(imageData) 
		{
			/*
			if(index == 1 )
			$scope.imgURI = "data:image/jpeg;base64," + imageData;
			else
			*/
			$scope.imgURI = imageData
			
			var myImg = $scope.imgURI;
			var options = new FileUploadOptions();
			options.mimeType = 'jpeg';
			options.fileKey = "file";
			options.fileName = $scope.imgURI.substr($scope.imgURI.lastIndexOf('/') + 1);
			//alert(options.fileName)
			var params = {};
			//params.user_token = localStorage.getItem('auth_token');
			//params.user_email = localStorage.getItem('email');
			options.params = params;
			var ft = new FileTransfer();
			ft.upload($scope.imgURI, encodeURI($rootScope.host+'/UploadImg1.php'), $scope.onUploadSuccess, $scope.onUploadFail, options);
    	});
		
		$scope.onUploadSuccess = function(data)
		{
			//alert (data);
			//alert (data.response)
			//data = JSON.stringify(data);
			//alert(data.response)
			if (data.response) {
				$scope.image = data.response;
				//$scope.showsubmit = false;
				$scope.uploadedimage = $rootScope.host+data.response;
			}
		}
		
		$scope.onUploadFail = function(data)
		{
			//alert("onUploadFail : " + data)
		}
    }
	
	
	$scope.saveAccident = function() 
	{ 
	
		$localStorage.accidentname =  $scope.accidentFields.name;
		$localStorage.accidentphone = $scope.accidentFields.phone;
		
		
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
	
		send_params = 
		{
		"send" : 1,
		"name" : $scope.accidentFields.name,
		"phone" : $scope.accidentFields.phone,
		"devision" : $scope.accidentFields.devision,
		"room" : $scope.accidentFields.room,
		"area" : $scope.accidentFields.areasSelect,
		"inventer" : $scope.accidentFields.inventer,
		"desc" : $scope.accidentFields.desc,
		"image" : $scope.image
		}
		//console.log(send_params)
		$http.post($rootScope.host+'/send_Accident.php',send_params)
		.success(function(data, status, headers, config)
		{
			//$scope.accidentFields.name = $scope.accidentFields.name;
			//$scope.accidentFields.phone = $scope.accidentFields.phone;
			//$scope.accidentFields.devision = '';
			$scope.accidentFields.room = '';
			$scope.accidentFields.areasSelect = '';
			$scope.accidentFields.inventer = '';
			$scope.accidentFields.desc = '';
			$scope.image = '';
			
			$ionicPopup.alert({
			 title: "פנייתך מספר "+data.response.id+" התקבלה במוקד 555.",
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
			});			
			
			window.location.href = "#/app/mainemployee";
		})
		.error(function(data, status, headers, config)
		{

		});

	}
	
})


.controller('EmlopyeeMonthController', function($scope,$http,$rootScope,$stateParams,$localStorage,$state,$ionicPlatform,$timeout)
{
	mixpanel.track("עמוד רופא החודש");

	if (window.cordova)
	{
	
		$timeout(function() {
			 
		 var eventName = "רופא החודש";
		 //var eventValues = {};
		 //window.plugins.appsFlyer.trackEvent(eventName);
		 }, 3000);
	}


		
	$scope.imageurl = $rootScope.host;

	$http.get($rootScope.host+'/getEmlopyeeMonth.php')
	.success(function(data, status, headers, config)
	{
		$scope.articleimage = data.response.image;
		$scope.articletitle = data.response.title;
		$scope.articletext = data.response.desc;
		
	})
	.error(function(data, status, headers, config)
	{

	});
	
})

.controller('AddTradeController', function($scope,$http,$cordovaCamera,$rootScope,$ionicActionSheet,$stateParams,$localStorage,$state,$ionicPopup,$ionicPlatform,$timeout)
{
	mixpanel.track("עמוד הוספת שוק תן וקח");

	if (window.cordova)
	{
	
		$timeout(function() {
		 
		 var eventName = "הוספת שוק תן וקח";
		 //var eventValues = {};
		 //window.plugins.appsFlyer.trackEvent(eventName);
		 }, 3000);
	}

		
	
	$scope.showsubmit = false;
	$scope.uploadedimage = 'img/main/camera.png';
	
	$scope.onFileSelect = function (file) 
	{
		var fd = new FormData();
        fd.append('fileUPP', file.files[0]);
		$scope.showsubmit = true;	
			
		$http.post($rootScope.host+'/UploadImg.php', fd, { transformRequest: angular.identity,"headers" : { "Content-Type" : undefined }}).success(function(data) 
		{
			if (data.response.imagepath) {
				$scope.image = data.response.imagepath;
				$scope.showsubmit = false;
				$scope.uploadedimage = $rootScope.host+data.response.imagepath;
			}
         	//alert("p: "+data)  
        }).error(function(data) 
		{
			//alert(data.message); 	
		});
	};
	
	
	
	$scope.CameraOption = function() 
	{
	   // Show the action sheet
	   var hideSheet = $ionicActionSheet.show({
		 buttons: [
		   { text: 'בחר תמונה מהגלרייה' },
		   { text: 'פתח מצלמה' },
		   { text: 'חזור'}
		 ],
		 cancelText: 'Cancel',
		 cssClass: 'social-actionsheet',
		 cancel: function() {
			  return true;
			},
		 buttonClicked: function(index) {
			 if(index<2)
			 	$scope.takePicture(index);
			 
		   return true;
		 }});
			 // For example's sake, hide the sheet after two seconds
	
		
	};
	

	// destinationType : Camera.DestinationType.DATA_URL,  CAMERA
	$scope.takePicture = function(index) {
		 var options ;
		 
		if(index == 1 )
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI, // Camera.DestinationType.DATA_URL, 
				sourceType : Camera.PictureSourceType.CAMERA, 
				allowEdit : false,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 600,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false
			};
		}
		else
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI, 
				sourceType : Camera.PictureSourceType.PHOTOLIBRARY, 
				allowEdit : false,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 600,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false
			};
		}
	 
        $cordovaCamera.getPicture(options).then(function(imageData) 
		{
			/*
			if(index == 1 )
			$scope.imgURI = "data:image/jpeg;base64," + imageData;
			else
			*/
			$scope.imgURI = imageData;
			//alert ($scope.imgURI)
			var myImg = $scope.imgURI;
			var options = new FileUploadOptions();
			options.mimeType = 'jpeg';
			options.fileKey = "file";
			options.fileName = $scope.imgURI.substr($scope.imgURI.lastIndexOf('/') + 1);
			
			/*
			options.chunkedMode = false;
			options.headers = {
			Connection: "close"
			};
			*/
   
			//alert(options.fileName)
			var params = {};
			//params.user_token = localStorage.getItem('auth_token');
			//params.user_email = localStorage.getItem('email');
			options.params = params;
			var ft = new FileTransfer();
			ft.upload($scope.imgURI, encodeURI($rootScope.host+'UploadImg1.php'), $scope.onUploadSuccess, $scope.onUploadFail, options);
    	});
		
		$scope.onUploadSuccess = function(data)
		{
			//data = JSON.stringify(data);
			//alert(data.response)
			//alert (data);
			//alert (data.response);
			if (data.response) {
				$scope.image = data.response;
				//alert ($scope.image)
				//$scope.showsubmit = false;
				//$scope.uploadedimage = $rootScope.host+data.response.imagepath;
			}
		}
		
		$scope.onUploadFail = function(error)
		{
			//alert("onUploadFail : " + error);
			//alert("An error has occurred: Code = " + error.code);
		
			//console.log("upload error source " + error.source);
			//alert("upload error target " + error.target);
			console.log("upload error target " + error.target);
			//console.log(data);
			
		}
    }
	
	$scope.saveProduct = function() 
	{
		
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		send_params = 
		{
			"user" : $localStorage.username,
			"title" : $scope.title,
			"price" : $scope.price,
			"desc" : $scope.desc,
			"phone" : $scope.phone,
			"image" : $scope.image
		}
		
		//alert ($scope.image)
		//console.log(send_params)
		$http.post($rootScope.host+'/save_Product.php',send_params)
		.success(function(data, status, headers, config)
		{
			$ionicPopup.alert({
			 title: "תודה מוצר נוסף בהצלחה",
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
			});	
			
			window.location.href = "#/app/trading";
		})
		.error(function(data, status, headers, config)
		{

		});
		
		
	}
	

})


.controller('ViewSaleController', function($scope,$http,$rootScope,$stateParams,$localStorage,$state,$ionicPlatform,$timeout)
{
	mixpanel.track("עמוד פרטי מוצר - שוק תן קח");

	if (window.cordova)
	{
	
		$timeout(function() {
		 
		 var eventName = "עמוד פרטי מוצר - שוק תן קח";
		 //var eventValues = {};
		 //window.plugins.appsFlyer.trackEvent(eventName);
		 }, 3000);
	}

		
	$scope.id = $stateParams.id;
	$scope.imageurl = $rootScope.host;

	//console.log(send_params)
	$http.post($rootScope.host+'/getTrading.php?id='+$scope.id)
	.success(function(data, status, headers, config)
	{
		$scope.image = data.response.image;
		$scope.title = data.response.title;
		$scope.price = data.response.price;
		$scope.desc = data.response.desc;
		$scope.username = data.response.username;
		$scope.phone = data.response.phone;

	})
	.error(function(data, status, headers, config)
	{

	});

})


.controller('GalleryController', function($scope,$http,$rootScope,$stateParams,$localStorage,$state,$ionicLoading,$ionicPlatform,$timeout)
{
	mixpanel.track("עמוד גלריית עובדים");

	if (window.cordova)
	{
	
		$timeout(function() {
		 var eventName = "עמוד גלריית עובדים";
		 //var eventValues = {};
		 //window.plugins.appsFlyer.trackEvent(eventName);
		 }, 3000);
	}
		
	
	$scope.$on('$ionicView.enter', function(e) 
	{
		
		$ionicLoading.show({
		  template: 'טוען...',
		  noBackdrop : true,
		  duration : 10000
		});
		
		$scope.imageurl = $rootScope.host;
		
		//console.log(send_params)
		$http.post($rootScope.host+'/getWorkerGallery.php')
		.success(function(data, status, headers, config)
		{

		$scope.gallery = data.response; 
		$ionicLoading.hide();


		})
		.error(function(data, status, headers, config)
		{
			$ionicLoading.hide();
		});
 });
})


.controller('AddGalleryController', function($scope,$http,$rootScope,$stateParams,$localStorage,$state,$ionicLoading,$ionicActionSheet,$cordovaCamera,$ionicPopup,$ionicPlatform,$timeout)
{
	mixpanel.track("עמוד הוספת גלריית עובדים");

	if (window.cordova)
	{
	
		$timeout(function() {
		 var eventName = "עמוד הוספת גלריית עובדים";
		 //var eventValues = {};
		 //window.plugins.appsFlyer.trackEvent(eventName);
		 }, 3000);
	}

	
	$scope.showsubmit = false;
	$scope.picturetaken = false;
	$scope.uploadedimage = 'img/main/camera.png';
	

	$scope.CameraOption = function() 
	{
	   // Show the action sheet
	   var hideSheet = $ionicActionSheet.show({
		 buttons: [
		   { text: 'בחר תמונה מהגלרייה' },
		   { text: 'פתח מצלמה' },
		   { text: 'חזור'}
		 ],
		 cancelText: 'Cancel',
		 cssClass: 'social-actionsheet',
		 cancel: function() {
			  return true;
			},
		 buttonClicked: function(index) {
			 if(index<2)
			 	$scope.takePicture(index);
			 
		   return true;
		 }});	
	};
	
   
	// destinationType : Camera.DestinationType.DATA_URL,  CAMERA
	$scope.takePicture = function(index) {
		 var options ;
		 
		if(index == 1 )
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI, //Camera.DestinationType.DATA_URL, 
				sourceType : Camera.PictureSourceType.CAMERA, 
				allowEdit : false,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 600,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
		else
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI, 
				sourceType : Camera.PictureSourceType.PHOTOLIBRARY, 
				allowEdit : false,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 600,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
	 
        $cordovaCamera.getPicture(options).then(function(imageData) 
		{	
		/*
			if(index == 1 )
			$scope.imgURI = "data:image/jpeg;base64," + imageData;
			else
		*/
			$scope.imgURI = imageData;
			//alert ($scope.imgURI)
			var myImg = $scope.imgURI;
			var options = new FileUploadOptions();
			options.mimeType = 'jpeg';
			options.fileKey = "file";
			options.fileName = $scope.imgURI.substr($scope.imgURI.lastIndexOf('/') + 1);
			//alert(options.fileName)
			var params = {};
			//params.user_token = localStorage.getItem('auth_token');
			//params.user_email = localStorage.getItem('email');
			options.params = params;
			var ft = new FileTransfer();
			ft.upload($scope.imgURI, encodeURI($rootScope.host+'/UploadImg1.php'), $scope.onUploadSuccess, $scope.onUploadFail, options);
    	});
		
		$scope.onUploadSuccess = function(data)
		{
			if (data.response) {
				$scope.image = data.response;
				$scope.picturetaken = true;
			}
		}
		
		$scope.onUploadFail = function(data)
		{
			//alert("onUploadFail : " + data)
		}
    }
	
	$scope.saveGallery = function() {
		
		if (!$scope.picturetaken)
		{
			$ionicPopup.alert({
			 title: "יש לבחור תמונה",
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
			});			
		}
		else 
		{
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
			send_params = {
					"user" : $localStorage.username,
					"title" : $scope.title,
					"image" : $scope.image,
					"send" : 1			
					}
				
				//alert ($scope.image)
				//console.log(send_params)
				$http.post($rootScope.host+'/save_Gallery.php',send_params)
				.success(function(data, status, headers, config)
				{

				})
				.error(function(data, status, headers, config)
				{

				});

					alert ("תמונה נוספה בהצלחה , ממתין לאישור צוות האפליקצייה");
					window.location.href = "#/app/mainemployee";				
		}
	
	}	
 
})

.controller('RidesMainCtrl', function($scope,$http,$rootScope,$stateParams,$localStorage,$state,$ionicLoading,$ionicActionSheet,$cordovaCamera,$ionicPopup,$ionicPlatform,$timeout)
{
	mixpanel.track("עמוד טרמפים ראשי");

	if (window.cordova)
	{
		$timeout(function() {
		 var eventName = "עמוד טרמפים ראשי";
		 //var eventValues = {};
		 //window.plugins.appsFlyer.trackEvent(eventName);
		 }, 3000);
	}
	
	$scope.getRides = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		send_params = 
		{
			"user" : $localStorage.username,
		}

		$http.post($rootScope.host+'/get_rides.php',send_params)
		.success(function(data, status, headers, config)
		{
			$rootScope.RidesArray = data;
		})
		.error(function(data, status, headers, config)
		{

		});		
	}
	
	$scope.getRides();
	
 
 
})


.controller('ManageRideCtrl', function($scope,$http,$rootScope,$stateParams,$localStorage,$state,$ionicLoading,$ionicActionSheet,$cordovaCamera,$ionicPopup,$ionicPlatform,$timeout)
{
	mixpanel.track("עמוד הוספת טרמפ");

	if (window.cordova)
	{
		$timeout(function() {
		 var eventName = "עמוד הוספת טרמפ";
		 //var eventValues = {};
		 //window.plugins.appsFlyer.trackEvent(eventName);
		 }, 3000);
	}
	
	$scope.fields = 
	{
		"user" : $localStorage.username,
		"fullname" : "",
		"phone" : "",
		"address" : "",
		"destination" : "",
		"departure_date" : "",
		"exit_time" : "",
		"return_time" : "",
		"remarks" : "",
		"seats" : "",
		"send" : 1
	}
	
	
	$scope.pickTime = function(type)
	{
		var options = {
			date: new Date(),
			mode: 'time',
			androidTheme: 2,
			is24Hour : true,
			okText : "אישור",
			cancelText: "ביטול"			
		};
		

		
		datePicker.show(options, function(date){
		  //alert("date result " + date);  
			$scope.newHour = date.toString().split(" ")[4];
			$scope.splitHour = $scope.newHour.split(":");
			$scope.newTime = $scope.splitHour[0]+":"+$scope.splitHour[1];
			
			//
			if (type == 0)
				$scope.fields.exit_time = $scope.newTime;
			else
				$scope.fields.return_time = $scope.newTime;
			
			$scope.updateTime(type);

		});

	}


	$scope.updateTime = function(type)
	{
		 $timeout(function() 
		 {
			 
			if (type == 0)
				$scope.fields.exit_time = $scope.newTime;
			else
				$scope.fields.return_time =  $scope.newTime;
		}, 100);
	}


	
	
	$scope.saveRide = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';


		$http.post($rootScope.host+'/add_ride.php',$scope.fields)
		.success(function(data, status, headers, config)
		{
			$ionicPopup.alert({
			 title: "הסעה נוספה בהצלחה",
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
			});	
			window.location.href = "#/app/rides";	
		})
		.error(function(data, status, headers, config)
		{

		});

	}

})	

.controller('RideDetailsCtrl', function($scope,$http,$rootScope,$stateParams,$localStorage,$state,$ionicLoading,$ionicActionSheet,$cordovaCamera,$ionicPopup,$ionicPlatform,$timeout)
{
	mixpanel.track("עמוד פרטי טרמפ");

	if (window.cordova)
	{
		$timeout(function() {
		 var eventName = "עמוד פרטי טרמפ";
		 //var eventValues = {};
		 //window.plugins.appsFlyer.trackEvent(eventName);
		 }, 3000);
	}
	
	$scope.RideId = $stateParams.ItemId;
	$scope.RideDetails = $rootScope.RidesArray[$scope.RideId];
	
	$scope.smsRide = function()
	{
		window.location.href = "sms:"+$scope.RideDetails.phone+"?body=הצטרפות נסיעה ל"+$scope.RideDetails.destination+" בתאריך "+$scope.RideDetails.departure_date+" בשעה "+$scope.RideDetails.exit_time;
	}
	
	
})	



.filter('getCatagoryImage', function($rootScope) 
{
	return function(data) 
	{
		var imgUrl = "";
		
		if(data)
		{
			imgUrl = $rootScope.host+data;
		}
		else
		{
			imgUrl = "img/new/iconlogo.png"
		}
		
		return imgUrl;
	};
})


.filter('cutString_ListPage', function () {
    return function (value, wordwise, max, tail) 
	{
		value =  value.replace(/(<([^>]+)>)/ig,"");
        if (!value) return '';
		
        max = parseInt(max, 10);
        if (!max) return value;
        if (value.length <= max) return value;

        value = value.substr(0, max);
    //    if (wordwise) {
            var lastspace = value.lastIndexOf(' ');
            if (lastspace != -1) {
                value = value.substr(0, lastspace);
            }
     //   }
        return value + (tail || ' - המשך לכתבה');
    };
})


.filter('cutString_TitlePage', function () {
    return function (value, wordwise, max, tail) 
	{
		value =  value.replace(/(<([^>]+)>)/ig,"");
        if (!value) return '';
		
        max = parseInt(max, 10);
        if (!max) return value;
        if (value.length <= max) return value;

        value = value.substr(0, max);
    //    if (wordwise) {
            var lastspace = value.lastIndexOf(' ');
            if (lastspace != -1) {
                value = value.substr(0, lastspace);
            }
     //   }
        return value + (tail || ' ');
    };
})


.filter('nl2p', function () {
    return function(text){
		
		//var newStr = text.replace(/(.{30})/g, "$1\n");
		 var newStr = text.replace(/.{20,30} /g, "$&\n")
		return newStr;
        //text = String(text).trim();
        //return (text.length > 0 ? '<p>' + text.replace(/[\r\n]+/g, '</p><p>') + '</p>' : null);
    }
})


.filter('formatDate', function () {
    return function(text){
	dateArray = text.split(' ');
	return dateArray[0];

    }
})

.filter('toTrusted', function ($sce) 
{
    return function (value) {
        return $sce.trustAsHtml(value);
    };
})

.directive('imageonload', function($ionicLoading,$rootScope) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            element.bind('load', function() {
                //alert('image is loaded');
				$rootScope.$broadcast('imageloaded')
            });
        }
    };
});
