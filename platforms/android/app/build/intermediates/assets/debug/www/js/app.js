// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers','ngStorage','ngCordova'])
.run(function($rootScope,$ionicPlatform,$http,$localStorage,$state,$ionicHistory,$timeout) {
	$rootScope.employee  = 0;
	$rootScope.host  = 'https://tapper.co.il/soroka/php/';
	//$rootScope.host  = 'https://sorokaapp.clalit.co.il/php/';
	//$rootScope.host  = 'https://sorokaapp.clalit.co.il/php/';

	$rootScope.selectedBlogCategory = 0;
	$rootScope.currState = $state;
	$rootScope.State = '';
	$rootScope.finshedLoading = 0;
	$rootScope.Blogs = '';
    $rootScope.loadedOnce = 0;
	$rootScope.CatType = 0;
	$rootScope.RidesArray = [];
	
	
	$rootScope.$watch('currState.current.name', function(newValue, oldValue) {
      $rootScope.State = newValue;
    });  

	$rootScope.newsItems = new Array();

	
	// load stories
	$rootScope.getStories = function()
	{
			$http.get($rootScope.host + 'getStories.php').then(function(resp) {
				//$ionicLoading.hide();
				$rootScope.Blogs = resp.data;
				console.log($rootScope.Blogs);
				$rootScope.finshedLoading = 1;
				
				$rootScope.MonthEmlopyee = 0;
				
				for (i =0; i < $rootScope.Blogs.length; i++)
				{
					if ($rootScope.Blogs[i].index == 1)
					{
						for (j = 0; j < $rootScope.Blogs[i].articles.length; j++)
						{
							if ($rootScope.MonthEmlopyee < parseInt($rootScope.Blogs[i].articles[j].index))
							{
								$rootScope.MonthEmlopyeePlace = j;							
							}
							
						}
					}
				}
				
				//alert ($rootScope.MonthEmlopyeePlace)
				
			console.log("Month : " + $rootScope.MonthEmlopyeePlace)
			//console.log($rootScope.MonthEmlopyee)
			//alert ($rootScope.Blogs)
			// For JSON responses, resp.data contains the result
			
			//Get Featured Articles
		
			$http.get($rootScope.host+'/getFeatured.php')
			.success(function(data, status, headers, config)
			{
				$rootScope.featuredArticles = data;
				$rootScope.newsItems = data;
				//$rootScope.newsItems.push(OB.news) =[{news:"hai"},{news:"hello"}];
				console.log("featured:" , $rootScope.featuredArticles)
				$rootScope.getAbout();
			})
			.error(function(data, status, headers, config)
			{
				$rootScope.getAbout();
			});



			
	  }, function(err) {
		console.error('ERR', err);
		// err.status will contain the status code
		})
		
	}
	
	$rootScope.getStories();

	
	///////About Load
	$rootScope.getAbout = function()
	{
		$http.get($rootScope.host + 'getAbout.php?id=1')
		.success(function(data, status, headers, config)
		{
			$rootScope.aboutimage = data.response.image;
			$rootScope.abouttitle = data.response.title;
			$rootScope.abouttext = data.response.desc;
			$rootScope.getContacts();
		})
		.error(function(data, status, headers, config)
		{
			$rootScope.getContacts();
		});		
	}


		
	///////Main Contacts Load	
	$rootScope.getContacts = function()
	{
		$http.get($rootScope.host+'/getContacts.php')
        .success(function(data, status, headers, config)
        {
			$rootScope.contacts = data.response;
			$rootScope.getNavigation();
		//	console.log($rootScope.contacts)
        })
        .error(function(data, status, headers, config)
        {
			$rootScope.getNavigation();
        });		
	}



	///////Main Navigation Load	
	
	$rootScope.getNavigation = function()
	{
		$http.get($rootScope.host+'/getNavigation.php')
        .success(function(data, status, headers, config)
        {
			$rootScope.navigation = data.response;
			$rootScope.getBuildings();
		//	console.log($rootScope.contacts)
        })
        .error(function(data, status, headers, config)
        {
			$rootScope.getBuildings();
        });		
	}
	



	///////Main Navigation Load	
	$rootScope.getBuildings = function()
	{
		$http.get($rootScope.host+'/getBuildings.php')
		.success(function(data, status, headers, config)
		{
			$rootScope.buildings = data.response;

		})
		.error(function(data, status, headers, config)
		{
			
		});			
	}



	
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

	
	
  });
})

.config(['$compileProvider', function($compileProvider){
 //$compileProvider.aHrefSanitizationWhitelist(/^\s*(geo|mailto|tel|maps):/);
 $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|geo|tel|maps):/);

}])
  
.config(function($stateProvider, $urlRouterProvider,$ionicConfigProvider) {
$ionicConfigProvider.backButton.previousTitleText(false).text('');

  $stateProvider
    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.search', {
    url: '/search',
    views: {
      'menuContent': {
        templateUrl: 'templates/search.html'
      }
    }
  })

  .state('app.browse', {
      url: '/browse',
      views: {
        'menuContent': {
          templateUrl: 'templates/browse.html'
        }
      }
    })

  
   .state('app.main', {
    url: '/main',
    views: {
      'menuContent': {
        templateUrl: 'templates/main.html',
        controller: 'MainController'
      }
    }
  })
  
  .state('app.categories', {
    url: '/categories',
    views: {
      'menuContent': {
        templateUrl: 'templates/menu_page.html',
        controller: 'MenuController'
      }
    }
  })
  
    .state('app.catagoryposts', {
    url: '/catagoryposts/:id',
    views: {
      'menuContent': {
        templateUrl: 'templates/blog_catagory_posts.html',
        controller: 'CatagoryPosts'
      }
    }
  })
  
      .state('app.blogpost/:id', {
    url: '/blogpost/:id',
    views: {
      'menuContent': {
        templateUrl: 'templates/blog_post.html',
        controller: 'BlogPost'
      }
    }
  })

        .state('app.about', {
    url: '/about',
    views: {
      'menuContent': {
        templateUrl: 'templates/about.html',
        controller: 'About'
      }
    }
  })
      .state('app.feedback', {
    url: '/feedback',
    views: {
      'menuContent': {
        templateUrl: 'templates/feedback.html',
        controller: 'Feedback'
      }
    }
  })
  
   .state('app.youtube', {
    url: '/youtube',
    views: {
      'menuContent': {
        templateUrl: 'templates/youtube.html',
        controller: 'Youtube'
      }
    }
  })
  
        .state('app.recommended', {
    url: '/recommended',
    views: {
      'menuContent': {
        templateUrl: 'templates/recommended.html',
        controller: 'Recommended'
      }
    }
  })
  
   .state('app.contacts', {
    url: '/contacts',
    views: {
      'menuContent': {
        templateUrl: 'templates/contacts.html',
        controller: 'Contacts'
      }
    }
  })

   .state('app.navigation', {
    url: '/navigation',
    views: {
      'menuContent': {
        templateUrl: 'templates/navigation.html',
        controller: 'Navigation'
      }
    }
  })

     .state('app.buildings', {
    url: '/buildings/:ItemId',
    views: {
      'menuContent': {
        templateUrl: 'templates/building.html',
        controller: 'NavigationBuilding'
      }
    }
  })
  
     .state('app.navigation_info', {
    url: '/navigation_info/:ItemId',
    views: {
      'menuContent': {
        templateUrl: 'templates/navigation_info.html',
        controller: 'NavigationInfo'
      }
    }
  })
  
  
   .state('app.login', {
    url: '/login',
    views: {
      'menuContent': {
        templateUrl: 'templates/login.html',
        controller: 'LoginCtrl'
      }
    }
  })
  
     .state('app.mainemployee', {
    url: '/mainemployee',
    views: {
      'menuContent': {
        templateUrl: 'templates/emlopyee_main.html',
        controller: 'EmployeeController'
      }
    }
  })
  
    .state('app.trading', {
    url: '/trading',
    views: {
      'menuContent': {
        templateUrl: 'templates/trading.html',
        controller: 'TradingController'
      }
    }
  })
  
      .state('app.addtrade', {
    url: '/addtrade',
    views: {
      'menuContent': {
        templateUrl: 'templates/add_trade.html',
        controller: 'AddTradeController'
      }
    }
  })
  
  
  .state('app.accident', {
    url: '/accident',
    views: {
      'menuContent': {
        templateUrl: 'templates/accident.html',
        controller: 'AccidentController'
      }
    }
  })
  
  
    .state('app.employeemonth', {
    url: '/employeemonth',
    views: {
      'menuContent': {
        templateUrl: 'templates/emlopyee_month.html',
        controller: 'EmlopyeeMonthController'
      }
    }
  })
  
      .state('app.viewsell', {
    url: '/viewsell/:id',
    views: {
      'menuContent': {
        templateUrl: 'templates/view_sell.html',
        controller: 'ViewSaleController'
      }
    }
  })

      .state('app.gallery', {
    url: '/gallery',
    views: {
      'menuContent': {
        templateUrl: 'templates/gallery.html',
        controller: 'GalleryController'
      }
    }
  })  

      .state('app.addgallery', {
    url: '/addgallery',
    views: {
      'menuContent': {
        templateUrl: 'templates/add_gallery.html',
        controller: 'AddGalleryController'
      }
    }
  })    
  

      .state('app.general_pdf', {
    url: '/general_pdf',
    views: {
      'menuContent': {
        templateUrl: 'templates/general_pdf.html',
        controller: 'GeneralPdfCtrl'
      }
    }
  })     
  
    .state('app.rides', {
    url: '/rides',
    views: {
      'menuContent': {
        templateUrl: 'templates/rides.html',
        controller: 'RidesMainCtrl'
      }
    }
  })       

    .state('app.manage_ride', {
    url: '/manage_ride',
    views: {
      'menuContent': {
        templateUrl: 'templates/manage_ride.html',
        controller: 'ManageRideCtrl'
      }
    }
  }) 


    .state('app.ride_details', {
    url: '/ride_details/:ItemId',
    views: {
      'menuContent': {
        templateUrl: 'templates/ride_details.html',
        controller: 'RideDetailsCtrl'
      }
    }
  }) 

  
  
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/main');
});
